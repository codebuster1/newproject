package week3.domain;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    // users - a list of users
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private ArrayList<User> users;
    private ArrayList<Teachers> teachers;
    private ArrayList<Student> students;
    private Teachers signedTeacher;
    private Student signedStudent;


    public MyApplication() throws IOException {
        users = new ArrayList<>();
        teachers = new ArrayList<>();
        students = new ArrayList<>();
    }

    private void addUser(User user) {
        users.add(user);
    }


    private void menu() throws FileNotFoundException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("(◕‿◕)");
                System.out.println("1. Authentication (͡° ͜ʖ ͡°)");
                System.out.println("2. Exit (>_<)");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                System.out.println("(◕‿◕)");
                System.out.println("1. More information about user");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) {
                    userProfile();
                } else break;

            }
        }
    }

    private void userProfile() {
        boolean isTeacher = false;
        for (Teachers teacher : teachers) {
            if (signedUser.getId() == teacher.getId()) {
                isTeacher = true;
                signedTeacher = teacher;
                break;
            }
        }
        if (isTeacher) {
            System.out.println(signedTeacher);
        } else {
            for (Student student : students) {
                if (signedUser.getId() == student.getId()) {
                    signedStudent = student;
                    break;
                }
            }
            System.out.println(signedStudent);
        }

    }

    private void logOff() {

    }

    private void authentication() throws FileNotFoundException {

        System.out.println("1.signIn");
        System.out.println("2.signUp");

        int choice = sc.nextInt();
        if (choice == 1)
            signIn();
        else if (choice == 2)
            signUp();
    }

    private void signIn() throws FileNotFoundException {
        System.out.println("Enter your Username: ");
        String usname = sc.next();
        boolean a = false;
        for (User user : users) {
            if (usname.equals(user.getUsername())) {
                a = true;
                break;
            }
        }
        while (true) {
            if (a) {
                System.out.println("Write your Password: ");
                String pass = sc.next();
                boolean b = false;
                for (User user : users) {
                    if (pass.equals(user.getPassword().getPassword())) {
                        b = true;
                        signedUser = user;

                    }
                }
                if (b) {
                    System.out.println("You're signed in!");
                } else {
                    System.out.println("Password is incorrect! Try again.");
                    pass = sc.next();
                }
            }
            break;
        }
    }


    private void signUp() {

        System.out.println("1.Teacher");
        System.out.println("2.Student");

        int choice = sc.nextInt();
        if (choice == 1)
            signUpTeacher();
        else if (choice == 2)
            signUpStudent();

    }

    public void signUpStudent() {
        System.out.println("Write your name: ");
        String name = sc.next();
        System.out.println("Write your surname: ");
        String surname = sc.next();
        System.out.println("Write username: ");
        String username = sc.next();
        System.out.println("Write faculty: ");
        String faculty = sc.next();
        System.out.println("If you have a stependya write 1 else 0:");
        boolean hasStepuha = (sc.nextInt() == 1) ? true : false;
        for (User user : users) {
            if (username.equals(user.getUsername())) {
                System.out.println("Write another Username: ");
                username = sc.next();
            } else {
                break;
            }
        }
        System.out.println("Write password :");
        String password = sc.next();
        Password p = new Password(password);

        while (!p.checkPassword(password)) {
            System.out.println("wrong PASSSSSSSWORD BUGA GA GA GAAAA");
            password = sc.next();
        }
        p.setPassword(password);
        User newuser = new User(name, surname, username, p);
        addUser(newuser);
        students.add(new Student(newuser, faculty, hasStepuha));
        signedUser = newuser;
    }

    public void storeStudents() throws FileNotFoundException {
        File file = new File("C:\\Users\\maga_\\IdeaProjects\\Application\\src\\com\\company\\dbStudents.txt");

        Scanner fsc = new Scanner(file);
        while (fsc.hasNext()) {
            int id = fsc.nextInt();
            String name = fsc.next();
            String surname = fsc.next();
            String username = fsc.next();
            Password password = new Password(fsc.next());
            String faculty = fsc.next();
            boolean hasStepuha = (fsc.nextInt() == 1) ? true : false;
            Student student = new Student(id, name, surname, username, password, faculty, hasStepuha);
            students.add(student);
        }
    }

    public void signUpTeacher() {
        System.out.println("Write your name: ");
        String name = sc.next();
        System.out.println("Write your surname: ");
        String surname = sc.next();
        System.out.println("Write username: ");
        String username = sc.next();
        System.out.println("Write salary: ");
        int salary = sc.nextInt();
        System.out.println("Write your subject:");
        String subject = sc.next();
        boolean us = false;
        for (User user : users) {
            if (username.equals(user.getUsername())) {
                System.out.println("Write another Username: ");
                username = sc.next();
            } else {
                us = true;
                break;
            }
        }
        System.out.println("Write password :");
        String password = sc.next();
        Password p = new Password(password);

        while (!p.checkPassword(password)) {
            System.out.println("wrong PASSSSSSSWORD BUGA GA GA GAAAA");
            password = sc.next();
        }
        p.setPassword(password);
        User newuser = new User(name, surname, username, p);
        addUser(newuser);
        teachers.add(new Teachers(newuser, salary, subject));
        signedUser = newuser;
    }


    public void start() throws IOException {
        File file = new File("C:\\Users\\maga_\\IdeaProjects\\Application\\src\\main\\java\\week3\\domain\\db.txt");
        storeTeachers();
        saveStudentsList();
        Scanner fsc = new Scanner(file);
        while (fsc.hasNext()) {
            int id = fsc.nextInt();
            String name = fsc.next();
            String surname = fsc.next();
            String username = fsc.next();
            Password password = new Password(fsc.next());
            User user = new User(id, name, surname, username, password);
            users.add(user);
        }
        while (true) {
            System.out.println("(｡❤‿❤｡) Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu (*^‿^*)");
            System.out.println("2. Exit (◣_◢)");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }
        for (User user : users) {
            System.out.println(user + "\n");
        }
        saveTeachersList();
        saveStudentsList();
        saveUserList();
    }


    public void storeTeachers() throws FileNotFoundException {
        File file = new File("C:\\Users\\maga_\\IdeaProjects\\Application\\src\\main\\java\\week3\\domain\\dbTeachers.txt");

        Scanner fsc = new Scanner(file);
        while (fsc.hasNext()) {
            int id = fsc.nextInt();
            String name = fsc.next();
            String surname = fsc.next();
            String username = fsc.next();
            Password password = new Password(fsc.next());
            int salary = fsc.nextInt();
            String subject = fsc.next();
            Teachers teacher = new Teachers(id, name, surname, username, password, salary, subject);
            teachers.add(teacher);
        }
    }

    public void saveStudentsList() throws IOException {
        String data = "";
        for (Student student : students) {
            data += student + "\n";
        }
        Files.write(Paths.get("C:\\Users\\maga_\\IdeaProjects\\Application\\src\\main\\java\\week3\\domain\\dbStudents.txt"), data.getBytes());
    }

    public void saveTeachersList() throws IOException {
        String data = "";
        for (Teachers teacher : teachers) {
            data += teacher + "\n";
        }
        Files.write(Paths.get("C:\\Users\\maga_\\IdeaProjects\\Application\\src\\main\\java\\week3\\domain\\dbTeachers.txt"), data.getBytes());
    }

    public void saveUserList() throws IOException {
        String data = "";
        for (User user : users) {
            data += user + "\n";
        }
        Files.write(Paths.get("C:\\Users\\maga_\\IdeaProjects\\Application\\src\\main\\java\\week3\\domain\\db.txt"), data.getBytes());
    }
}



