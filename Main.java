package week3.domain;
/*
dbTeachers.txt = 1 Mugzar Akimzhan akim MyPa$$word123 1000 Soft
dbStudents.txt = 3 Maga Maga Maga MyPa$$word123 Soft true
db = 1 Mugzar Akimzhan akim MyPa$$word123
3 Maga Maga Maga MyPa$$word123

*/
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Main {
    private static ArrayList<User> users = new ArrayList<>();

    public static void main(String[] args) throws IOException {
//        MyApplication application = new MyApplication();
//        System.out.println("An application is about to start..");
//        application.start();
        MyApplication app = new MyApplication();
        app.start();
    }
}
