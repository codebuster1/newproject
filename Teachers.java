package week3.domain;


public class Teachers extends User {
    private int salary;
    private String subject;

    Teachers(int id, String name, String surname, String username, Password password, int salary, String subject) {
        super(id, name, surname, username, password);
        setSalary(salary);
        setSubject(subject);
    }

    Teachers(User user , int salary , String  subject){
        super(user.getId(), user.getName(),user.getSurname(),user.getUsername(),user.getPassword());
        setSalary(salary);
        setSubject(subject);
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }


    @Override
    public String toString() {
        return  super.toString()+" "+ salary +" " + subject ;

    }
}
