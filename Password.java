package week3.domain;


public class Password {
    private String password;

    Password(String password) {
        setPassword(password);
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public boolean checkPassword(String passwordStr) {
        int countD = 0, countLC = 0, countUC = 0;
        if (passwordStr.length() > 9) {
            for (int i = 0; i < passwordStr.length(); i++) {
                if (countD > 0 && countLC > 0 && countUC > 0) {
                    return true;
                } else {
                    if (Character.isDigit(passwordStr.charAt(i))) {
                        countD++;
                    } else if (Character.isLowerCase(passwordStr.charAt(i))) {
                        countLC++;
                    } else if (Character.isUpperCase(passwordStr.charAt(i))) {
                        countUC++;
                    }
                }
            }
        }
        return false;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return password;
    }
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
}