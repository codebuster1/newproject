package week3.domain;


public class User {
    private static int id_gen = 0;
    private int id;
    private String name, surname, username;
    private Password password;

    public User() {
    }

    private void Id_gen() {
        this.id = id_gen+1;
    }

    public void setId_gen(int id) {
        if (id > id_gen)
            id_gen = id + 1;
    }

    User(int id, String name, String surname, String username, Password password) {
        setId(id);
        setId_gen(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    User(String name, String surname, String username, Password password) {
        Id_gen();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    void setId(int id) {
        this.id = id;
    }

    void setName(String name) {
        this.name = name;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    void setUsername(String username) {
        this.username = username;
    }

    void setPassword(Password password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password;
    }


    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password
}