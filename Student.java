package week3.domain;


public class Student extends User {
    private String faculty;
    private boolean stependeya;

    Student() {
        faculty = null;
        stependeya = false;
    }

    Student(User user,String faculty , boolean stependeya){
        super(user.getId(),user.getName(),user.getSurname(),user.getUsername(),user.getPassword());
        setFaculty(faculty);
        setStependeya(stependeya);
    }

    Student(int id, String name, String surname, String username, Password password, String faculty, boolean String){
        super(id, name, surname, username, password);
        setFaculty(faculty);
        setStependeya(stependeya);
    }

    Student(String name, String surname, String username, Password password, String  faculty,boolean stependeya){
        super(name, surname, username, password);
        setFaculty(faculty);
    }


    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setStependeya(boolean stependeya){this.stependeya=stependeya;}

    public boolean isStependeya(boolean stependeya){
        return stependeya;
    }

    @Override
    public String toString() {
        return   super.toString()+" "+ faculty+" "+
                stependeya ;
    }
}
